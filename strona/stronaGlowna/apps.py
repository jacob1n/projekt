from django.apps import AppConfig


class StronaGlownaConfig(AppConfig):
    name = 'stronaGlowna'
