from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, get_user_model, views
from django.views import View
from django import forms
from django.views.generic import View, TemplateView
from .forms import UserForm
from kolejki.models import Queue
from django.contrib.auth.models import Group
from datetime import datetime


def Index(request):
    template_name = "stronaGlowna/index.html"
    currDate = datetime.now().date()
    queue = Queue.objects.all()
    que=Queue() 

    for q in queue:
        if(q.startDate <= currDate and q.finishDate >= currDate):
            que = q.match_set.order_by('matchDate') 
            break 
    return render(request,template_name,{'queue': que, 'currentDate':datetime.now()})

   


class UserFormView(View):
    form_class = UserForm
    template_name = 'stronaGlowna/registration_form.html'

    # new account
    def get(self, request):
        form = self.form_class(None)
        return render(request, self.template_name, {'form': form})

    # process registration
    def post(self, request):
        form = self.form_class(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            confirmation = form.cleaned_data.get("confirm_password")
            if confirmation == password:
                user.set_password(password)
                if(not Group.objects.filter(name='User').exists()):
                    g = Group(name='User')
                    g.save()
                g = Group.objects.get(name='User')
                user.save()
                g.user_set.add(user)
                user = authenticate(username=username, password=password)
                if user is not None:
                    if user.is_active:
                        login(request, user)
                        return redirect('stronaGlowna:index')
            else:
                raise forms.ValidationError("Passwords don't match")
                # return render_to_response("registration_form.html", {'invalid': True })
        return render(request, self.template_name, {'form': form})
