from django.conf.urls import url
from . import views
from django.contrib.auth import views as auth_views

app_name = 'stronaGlowna'

urlpatterns = [
    # /
    url(r'^$', views.Index, name='index'),
    url(r'^login/$', auth_views.login, name='login'),
    url(r'^logout/$', auth_views.logout,
        {'template_name': 'logged_out.html'}, name='logout'),
    url(r'^register/$', views.UserFormView.as_view(), name='register')
]
