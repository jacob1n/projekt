from django.db import models
from teams.models import Team
from django.core.urlresolvers import reverse

# Create your models here.


class Queue(models.Model):
    startDate = models.DateField()
    finishDate = models.DateField()

    def get_absolute_url(self):
        return reverse('kolejki:details', kwargs={'pk': self.pk})

    def __str__(self):
        return str(self.startDate) + " -  " + str(self.finishDate)


class Match(models.Model):
    queue = models.ForeignKey(Queue)
    matchDate = models.DateTimeField()

    home = models.ForeignKey(Team, related_name='gospodarz')
    away = models.ForeignKey(Team, related_name='gosc')

    awayGoals = models.IntegerField(default=0)
    homeGoals = models.IntegerField(default=0)

    commentary = models.TextField(max_length=500, default="")
    # def create(cls,queue,matchDate,home,away):
    #     match = cls(queue=queue,matchDate=matchDate,home=home,away=away)
    #     return match
    # def __init__(self,queue,matchDate,home,away):
    #     super(Match, self).__init__()
    #     self.queue = queue
    #     self.matchDate = matchDate
    #     self.home = home
    #     self.away = away

    def __str__(self):
        return str(self.home) + " - " + str(self.away)

    def get_absolute_url(self):
        return reverse('kolejki:matchdetails', kwargs={'pk': self.pk})
