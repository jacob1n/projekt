from django.views import generic
from kolejki.models import Queue, Match
from django.urls import reverse_lazy
from .models import Match
from teams.models import Team, Player
from django.shortcuts import get_object_or_404, render
from django.shortcuts import redirect
from .forms import GoalForm
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from django.contrib import messages
from statistic.models import StatisticsPlayer
from itertools import chain
from stronaGlowna import views
from random import randint
from django.utils import timezone
from django.views import View
from statistic.views import UpdateTeamStatistics
from teams.views import updateStatistics
from datetime import datetime
from datetime import timedelta
# Create your views here.


class IndexView(generic.ListView):
    template_name = 'kolejki/index.html'
    context_object_name = 'allQueues'

    def get_queryset(self):
        return Queue.objects.all()


class QueueDetails(generic.DetailView):
    model = Queue
    template_name = "kolejki/details.html"
    def get_queryset(self):
        return Queue.objects.all()
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(object=self.object)
        return render(request, self.template_name, {
                'queue': context,
                'currentDate': datetime.now(),
            })


class QueueCreate(generic.CreateView):
    model = Queue
    fields = ['startDate', 'finishDate']


class QueueUpdate(generic.UpdateView):
    model = Queue
    fields = ['startDate', 'finishDate']


class QueueDelete(generic.DeleteView):
    model = Queue
    fields = ['startDate', 'finishDate']
    success_url = reverse_lazy('kolejki:index')


class MatchIndex(generic.ListView):
    template_name = 'kolejki/matchIndex.html'
    context_object_name = 'allMatches'

    def get_queryset(self):
        return Match.objects.all()


class MatchDetails(generic.DetailView):
    model = Match
    template_name = "kolejki/matchdetails.html"

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(object=self.object)
        return render(request, self.template_name, {
                'match': context,
                'playerStatistics': StatisticsPlayer.objects.all(),
            })


class MatchCreate(generic.CreateView):
    model = Match
    fields = ['home', 'away', 'queue', 'matchDate', 'commentary']


class MatchUpdate(generic.UpdateView):
    model = Match
    fields = ['home', 'away', 'queue', 'matchDate', 'commentary']


def QueueGenerate(request):
    if request.method == 'GET':
        return render(request, 'kolejki/queue-generate.html')
    else:
        Queue.objects.all().delete()
        teams = Team.objects.all()
        #teams = [1,2,3,4,5,6,7,8]
        matches = []
        date = datetime.now()
        #date = datetime.strptime("2017-05-05", "%Y-%m-%d")
        for round in create_schedule(list(teams)):
            queue = Queue()
            queue.startDate = date
            delay = timedelta(days=7)
            finish = date + delay
            queue.finishDate = finish
            queue.save()
            for team in round:
                day_delay = timedelta(days=2)
                hours = timedelta(hours=18)
                matchday = date + randint(0, 3) * day_delay + hours
                home = team[0]
                away = team[1]
                match = Match()
                match.queue = queue
                match.matchDate = matchday
                match.home = home
                match.away = away
                match.save()
            date = date + delay
        return redirect('kolejki:index')
       # return redirect('kolejki:index')


class MatchDelete(generic.DeleteView):
    model = Match
    fields = ['home', 'away', 'queue', 'matchDate', 'commentary']
    success_url = reverse_lazy('kolejki:index')


class UpdatePlayerStatistics():
    hgoals = 0
    agoals = 0

    def save_answer(self, request, match, player_list, stats):
        #print ((stats[0][0] ) )
        p = player_list[int(stats[0][0][5::1])]
        if(StatisticsPlayer.objects.filter(name=p.name, surname=p.surname, team_id=p.team.pk, match_id=match.pk).exists()):
            sp = StatisticsPlayer.objects.get(
                name=p.name, surname=p.surname, team_id=p.team.pk, match_id=match.pk)
        else:
            sp = StatisticsPlayer(
                name=p.name, surname=p.surname, team_id=p.team.pk, match_id=match.pk)
        sp.goals = stats[0][1]
        sp.yellow_cards = stats[1][1]
        sp.red_cards = stats[2][1]
        sp.save()

    def check_answer(self, request, match, player_list, stats):
        #print ((stats[0][0] ) )
        p = player_list[int(stats[0][0][5::1])]
        if(StatisticsPlayer.objects.filter(name=p.name, surname=p.surname, team_id=p.team.pk, match_id=match.pk).exists()):
            sp = StatisticsPlayer.objects.get(
                name=p.name, surname=p.surname, team_id=p.team.pk, match_id=match.pk)
        else:
            sp = StatisticsPlayer(
                name=p.name, surname=p.surname, team_id=p.team.pk, match_id=match.pk)

        if(match.home.pk == sp.team_id):
            self.hgoals += int(stats[0][1])
        else:
            self.agoals += int(stats[0][1])

    def DataMatchUpdate(self, request, pk):
        # if this is a POST request we need to process the form data
        match = get_object_or_404(Match, pk=pk)
        try:
            h = match.home.player_set.all()
            a = match.away.player_set.all()
        except (KeyError, Player.DoesNotExist):
            return render(request, 'kolejki/data.html', {
                'match': match,
                'error_message': "Player does not exist"
            })
        player_list = list(chain(h, a))
        form = GoalForm(request.POST or None, extra=player_list)
        self.hgoals = 0
        self.agoals = 0
        if form.is_valid():
            match.homeGoals = form.cleaned_data['homeGoals']
            match.awayGoals = form.cleaned_data['awayGoals']
        # for item in request.POST.keys():
            #print(  item + ' - ' + request.POST[item])
            for (goal, value1) in form.extra_g():
                for (yellow, value2) in form.extra_y():
                    for (red, value3) in form.extra_r():
                        stats = ((goal, value1),
                                 (yellow, value2), (red, value3))
                        self.check_answer(request, match, player_list, stats)
                        break
                    break 
            if(self.hgoals == match.homeGoals and self.agoals == match.awayGoals):
                for (goal, value1) in form.extra_g():
                    for (yellow, value2) in form.extra_y():
                        for (red, value3) in form.extra_r():
                            stats = ((goal, value1),
                                     (yellow, value2), (red, value3))
                            self.save_answer(
                                request, match, player_list, stats)
                            break
                        break
                match.save()
            else:

                return render(request, 'kolejki/data.html', {
                    'match': match,
                    'error_message': "Nieprawidłowe dane"
                })
            # create a form instance and populate it with data from the request:
            # check whether it's valid:

            # check whether it's valid:

                # process the data in form.cleaned_data as required
                # ...
                # redirect to a new URL:
            
            UpdateTeamStatistics()
            updateStatistics()
            return redirect('statistic:match-player-stats')
        # if a GET (or any other method) we'll create a blank form

        return render(request, 'kolejki/data.html', {'form': form, 'match': match})


def create_schedule(list):
    """ Create a schedule for the teams in the list and return it"""
    s = []

    #if len(list) % 2 == 1: list = list + ["BYE"]

    for i in range(len(list) - 1):

        mid = len(list) / 2
        l1 = list[:int(mid)]
        l2 = list[int(mid):]
        l2.reverse()

        # Switch sides after each round
        if(i % 2 == 1):
            s = s + [zip(l1, l2)]
        else:
            s = s + [zip(l2, l1)]

        list.insert(1, list.pop())

    return s
