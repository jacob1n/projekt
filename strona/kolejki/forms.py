from django import forms
from .models import Match, Queue
from teams.models import Team, Player


class GoalForm(forms.Form):
    homeGoals = forms.IntegerField(label='Home Goals:', min_value=0, initial=0)
    awayGoals = forms.IntegerField(label='Away Goals:', min_value=0, initial=0)

    def __init__(self, *args, **kwargs):
        extra = kwargs.pop('extra')
        super(GoalForm, self).__init__(*args, **kwargs)
        for i, fields in enumerate(extra):
            self.fields['goal_%s' % i] = forms.IntegerField(
                label=str(fields) + " Goals:", min_value=0, initial=0)
            self.fields['yellow_%s' % i] = forms.IntegerField(
                label="Yellow cards", min_value=0, max_value=2, initial=0)
            self.fields['red_%s' % i] = forms.IntegerField(
                label="Red cards", min_value=0, max_value=1, initial=0)

    def extra_g(self):
        for name, value in self.cleaned_data.items():
            if name.startswith('goal_'):
                yield (name, value)

    def extra_y(self):
        for name, value in self.cleaned_data.items():
            if name.startswith('yellow_'):
                yield (name, value)

    def extra_r(self):
        for name, value in self.cleaned_data.items():
            if name.startswith('red_'):
                yield (name, value)
