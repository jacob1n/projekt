# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-05-14 17:39
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('teams', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Match',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('matchDate', models.DateTimeField()),
                ('awayGoals', models.IntegerField(default=0)),
                ('homeGoals', models.IntegerField(default=0)),
                ('commentary', models.TextField(default='', max_length=500)),
                ('away', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='gosc', to='teams.Team')),
                ('home', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='gospodarz', to='teams.Team')),
            ],
        ),
        migrations.CreateModel(
            name='Queue',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('startDate', models.DateField()),
                ('finishDate', models.DateField()),
            ],
        ),
        migrations.AddField(
            model_name='match',
            name='queue',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='kolejki.Queue'),
        ),
    ]
