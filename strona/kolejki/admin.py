from django.contrib import admin
from .models import Match, Queue
# Register your models here.
admin.site.register(Match)
admin.site.register(Queue)
