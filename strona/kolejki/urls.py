from django.conf.urls import url
from kolejki import views

app_name = 'kolejki'

urlpatterns = [
    # /kolejki/
    url(r'^$', views.IndexView.as_view(), name='index'),
    # /kolejki/<id>
    url(r'^(?P<pk>[0-9]+)/$', views.QueueDetails.as_view(), name="details"),
    # /kolejki/add
    url(r'^add/$', views.QueueCreate.as_view(), name='queue-add'),
    # /kolejki/<id>/update
    url(r'^(?P<pk>[0-9]+)/update/$',
        views.QueueUpdate.as_view(), name='updateView'),
    # /kolejki/<id>/delete
    url(r'^(?P<pk>[0-9]+)/delete/$',
        views.QueueDelete.as_view(), name='DeleteView'),
    # /kolejki/matches
    url(r'^matches/$', views.MatchIndex.as_view(), name='MatchIndex'),
    # /kolejki/matches/<id>
    url(r'^matches/(?P<pk>[0-9]+)/$',
        views.MatchDetails.as_view(), name="matchdetails"),
    # /kolejki/matches/<id>/data
    url(r'^matches/(?P<pk>[0-9]+)/data/$',
        views.UpdatePlayerStatistics().DataMatchUpdate, name="match-data-update"),
    # /kolejki/matches/add
    url(r'^matches/add/$', views.MatchCreate.as_view(), name='match-add'),
    # /kolejki/matches/<id>/delete
    url(r'^matches/(?P<pk>[0-9]+)/update/$',
        views.MatchUpdate.as_view(), name='matchupdate'),
    # /kolejki/<id>/delete
    url(r'^matches/(?P<pk>[0-9]+)/delete/$',
        views.MatchDelete.as_view(), name='matchdelete'),
    # /kolejki/generate
    url(r'^generate/$', views.QueueGenerate, name="queue-generate")
]
