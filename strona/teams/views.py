from django.views import generic
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from .models import Team, Player
from statistic.models import StatisticsPlayer
# Create your views here.


class IndexView(generic.ListView):
    template_name = 'teams/index.html'
    context_object_name = 'all_teams'

    def get_queryset(self):
        return Team.objects.all()


class DetailView(generic.DetailView):
    model = Player
    template_name = 'teams/detail.html'

    def get_queryset(self):
        return Team.objects.all()


class CreateTeam(CreateView):
    model = Team
    fields = ['name']


class UpdateTeam(UpdateView):
    model = Team
    fields = ['name']


class DeleteTeam(DeleteView):
    model = Team
    success_url = reverse_lazy('teams:index')


class CreatePlayer(CreateView):
    model = Player
    fields = ['name', 'surname', 'team']
    success_url = reverse_lazy('teams:index')

class UpdatePlayer(UpdateView):
    model = Player
    fields = ['name', 'surname', 'team']


class DeletePlayer(DeleteView):
    model = Player
    success_url = reverse_lazy('teams:index')


def updateStatistics(): 
            plist=Player.objects.all()
            pslist=StatisticsPlayer.objects.all()
            for p in plist : 
                p.goals = 0
                p.yellow_cards = 0
                p.red_cards =0
                p.second_yellow_cards = 0
                for ps in pslist:
                    if p.name==ps.name and p.surname ==ps.surname and p.team.id==ps.team_id :
                        p.goals=p.goals+ps.goals
                        p.yellow_cards=p.yellow_cards+ps.yellow_cards
                        p.red_cards=p.red_cards+ps.red_cards
                        p.second_yellow_cards=p.second_yellow_cards+ps.second_yellow_cards
                p.save()
            return 

