from django.db import models
from django.core.urlresolvers import reverse


class Team(models.Model):
    name = models.TextField(max_length=50)
    matches = models.IntegerField(default=0)
    win = models.IntegerField(default=0)
    draw = models.IntegerField(default=0)
    lose = models.IntegerField(default=0)
    goals = models.IntegerField(default=0)
    points = models.IntegerField(default=0)

    def get_absolute_url(self):
        return reverse('teams:detail', kwargs={'pk': self.pk})

    def __str__(self):
        return self.name


class Player(models.Model):
    name = models.TextField(max_length=50)
    surname = models.TextField(max_length=50)
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    goals = models.IntegerField(default=0)
    yellow_cards = models.IntegerField(default=0)
    red_cards = models.IntegerField(default=0)
    second_yellow_cards = models.IntegerField(default=0)

    def get_absolute_url(self):
        return reverse('teams:detail', kwargs={'pk': self.pk})

    def __str__(self):
        return self.team.name + " - " + self.name + " " + self.surname + " "
