from django.conf.urls import url
from . import views

app_name = "teams"

urlpatterns = [
    #/teams/
    url(r'^$', views.IndexView.as_view(), name='index'),
    #/teams/1/
    url(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),
    #/teams/add/
    url(r'^add/$', views.CreateTeam.as_view(), name='team-add'),
    #/teams/1/update/
    url(r'^(?P<pk>[0-9]+)/$', views.UpdateTeam.as_view(), name='team-update'),
    #/teams/1/delete/
    url(r'^(?P<pk>[0-9]+)/delete/$',
        views.DeleteTeam.as_view(), name='team-delete'),
    #/teams/player/add/
    url(r'^player/add/$', views.CreatePlayer.as_view(), name='player-add'),
    #/teams/player/1/update/
    url(r'^player/(?P<pk>[0-9]+)/update/$',
        views.UpdatePlayer.as_view(), name='player-update'),
    #/teams/player/1/delete/
    url(r'^player/(?P<pk>[0-9]+)/delete/$',
        views.DeletePlayer.as_view(), name='player-delete'),

]
