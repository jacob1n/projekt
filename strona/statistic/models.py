# Create your models here.
from django.db import models


class StatisticsPlayer(models.Model):
    name = models.TextField(max_length=50)
    surname = models.TextField(max_length=50)
    team_id = models.IntegerField()
    match_id = models.IntegerField()
    goals = models.IntegerField(default=0)
    yellow_cards = models.IntegerField(default=0)
    red_cards = models.IntegerField(default=0)
    second_yellow_cards = models.IntegerField(default=0)

    def __str__(self):
        return str(self.name) + " -  "
