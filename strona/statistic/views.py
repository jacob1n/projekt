from django.views import View, generic
from django.views.generic import TemplateView, DetailView
from teams.models import Team, Player
from kolejki.models import Match
from .models import StatisticsPlayer
from django.shortcuts import get_object_or_404, render
from django.shortcuts import redirect
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseBadRequest
from django.urls import reverse
from django import forms
from django.utils.translation import gettext as _
from django.contrib import messages
from datetime import datetime


class Index(TemplateView):
    template_name = "statistic/index.html"


def Players(request):
    template_name = "statistic/players.html"
    players = Player.objects.all().order_by('-goals')
    context = {'player_list': players, }
    return render(request, template_name, context)


def Teams(request):
    template_name = "statistic/teams.html"
    teams = Team.objects.all().order_by('-points')
    context = {'team_list': teams, }
    return render(request, template_name, context)


class DetailStatisticView(generic.ListView):
    model = StatisticsPlayer
    template_name = 'statistic/statisticPlayer.html'
    context_object_name = 'all_stats'

    def get_queryset(self):
        return StatisticsPlayer.objects.all()


def UpdateTeamStatistics():
    teamlist = Team.objects.all()
    matchlist = Match.objects.all()
    for team in teamlist:
        team.matches = 0
        team.win = 0
        team.draw = 0
        team.lose = 0
        team.goals = 0
        team.points = 0
        for match in matchlist.filter(home=team):
            if datetime.today() > match.matchDate:
                team.matches = team.matches + 1
                team.goals = team.goals + match.homeGoals
                if match.homeGoals > match.awayGoals:
                    team.win = team.win + 1
                    team.points = team.points + 3
                elif match.homeGoals == match.awayGoals:
                    team.draw = team.draw + 1
                    team.points = team.points + 1
                else:
                    team.lose = team.lose + 1

        for match in matchlist.filter(away=team):
            if datetime.today() > match.matchDate:
                team.matches = team.matches + 1
                team.goals = team.goals + match.awayGoals
                if match.awayGoals > match.homeGoals:
                    team.win = team.win + 1
                    team.points = team.points + 3
                elif match.homeGoals == match.awayGoals:
                    team.draw = team.draw + 1
                    team.points = team.points + 1
                else:
                    team.lose = team.lose + 1
        team.save()
    return
