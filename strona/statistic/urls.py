from django.conf.urls import url
from statistic import views

app_name = 'statistic'

urlpatterns = [
    # /statistic/
    url(r'^$', views.Index.as_view(), name='statistic-index'),
    # /statistic/players
    url(r'^players/$', views.Players, name='statistic-players'), 
    # /statistic/teams
    url(r'^teams/$', views.Teams, name='statistic-teams'),
    #/edycja/1..
    url(r'^a/$', views.DetailStatisticView.as_view(), name='match-player-stats'),
]
